package com.moderocky.cartographer;

import com.moderocky.cartographer.internal.web.MapServer;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StandaloneMapDisplay {

    private static StandaloneMapDisplay mapper;
    private MapServer server;

    private StandaloneMapDisplay(int port) {
        System.out.println("Opening a new display server on port: " + port);
        server = new MapServer(port, "plugins/Cartographer/web/");
    }

    public static void main(String[] args) throws IOException {
        boolean runnable = true;
        int port;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                return;
            }
        } else {
            port = 8500;
        }

        mapper = new StandaloneMapDisplay(port);

        while (runnable) {
            if (System.in == null) continue;
            String string = getInput();
            if (string.equalsIgnoreCase("stop"))
                runnable = false;
            if (string.equalsIgnoreCase("port"))
                System.out.println("The webserver port is: " + port);
        }
    }

    private static String getInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder builder = new StringBuilder();
        String string;
        while ((string = reader.readLine()) != null) {
            builder.append(string);
        }
        return builder.toString();
    }

    public static StandaloneMapDisplay getMapper() {
        return mapper;
    }

    @NotNull
    public MapServer getServer() {
        return server;
    }
}
