package com.moderocky.cartographer;

import com.moderocky.cartographer.api.MapAPI;
import com.moderocky.cartographer.internal.blind.BlindDataInterface;
import com.moderocky.cartographer.internal.command.CartographerCommand;
import com.moderocky.cartographer.internal.command.MapCommand;
import com.moderocky.cartographer.internal.config.PluginConfig;
import com.moderocky.cartographer.internal.map.Palette;
import com.moderocky.cartographer.internal.web.MapServer;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import com.moderocky.mask.internal.utility.FileManager;
import com.moderocky.mask.template.Plugin;
import org.bstats.bukkit.Metrics;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.Socket;
import java.util.logging.Level;

public final class Cartographer extends Plugin {

    private static final @NotNull MapAPI mapAPI = new MapAPI();
    private static Cartographer instance;
    private final @NotNull PluginConfig config = new PluginConfig();
    private final @NotNull Metrics metrics = new Metrics(this, 6972);
    private MapServer server;
    private Palette palette;
    private BlindDataInterface dataInterface;

    public static Cartographer getInstance() {
        return instance;
    }

    public static MapAPI getAPI() {
        return mapAPI;
    }

    @Override
    public void startup() {
        instance = this;
        try {
            dataInterface = (BlindDataInterface) Class.forName(Cartographer.class.getPackage().getName() + ".internal.blind." + getCraftVersion() + ".DataInterface").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            getLogger().log(Level.SEVERE, "* WARNING");
            getLogger().log(Level.SEVERE, "* Cartographer does not support this version.");
            getLogger().log(Level.SEVERE, "* Basically nothing will work lol.");
            e.printStackTrace();
        }
        createPalette();
        register(new CartographerCommand(), new MapCommand());
        registerSyntax();
        if (config.enableWebMap) {
            server = new MapServer(config.webMapPort, "plugins/Cartographer/web/");
            server.start();
        }
    }

    protected void createPalette() {
        File file = new File("plugins/Cartographer/", "colours.yml");
        if (!file.exists()) {
            FileManager.putIfAbsent(file);

        }
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        palette = new Palette(configuration);
    }

    public Palette getPalette() {
        return palette;
    }

    @Override
    public void disable() {
        instance = null;
        server.shutDown();
    }

    @NotNull
    public Metrics getMetrics() {
        return metrics;
    }

    @NotNull
    public PluginConfig getPluginConfig() {
        return config;
    }

    public BlindDataInterface getDataInterface() {
        return dataInterface;
    }
}
