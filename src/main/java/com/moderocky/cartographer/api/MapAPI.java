package com.moderocky.cartographer.api;

import com.moderocky.cartographer.Cartographer;
import com.moderocky.cartographer.internal.map.*;
import com.moderocky.cartographer.internal.map.template.Map;
import com.moderocky.mask.annotation.API;
import com.moderocky.mask.annotation.Asynchronous;
import com.moderocky.mask.internal.utility.FileManager;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.NotNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@API
public class MapAPI {

    private final int maxConcurrent = 12;
    private final ExecutorService threads = Executors.newFixedThreadPool(maxConcurrent);
    private volatile int inProgress = 0;

    /**
     * @return the material -> colour palette used for map generation.
     */
    public Palette getPalette() {
        return Cartographer.getInstance().getPalette();
    }

    /**
     * Reloads the in-use palette from the colours.yml file.
     */
    public void reloadPalette() {
        getPalette().regenerate();
    }

    /**
     * Regenerates the entire palette.
     * Overwrites any existing changes.
     * Requires the world to be loaded, this places blocks and gathers situational data from them.
     */
    public void regeneratePalette() {
        getPalette().generate();
    }

    /**
     * This will generate any missing, black or non-existent entries.
     * This could be used to generate blocks from a new version without overwriting any changes made to the palette.
     */
    public void regeneratePaletteSafely() {
        getPalette().generateSafely();
    }

    /**
     * @return The number of maps in progress
     */
    public int getInProgress() {
        return inProgress;
    }

    public synchronized void addProgressor() {
        inProgress++;
    }

    public synchronized void removeProgressor() {
        inProgress--;
    }

    /**
     * Creates a map, with an image consumer to deal with the image on completion.
     * This consumer will (currently) be run regularly during generation to store partials of the image.
     *
     * @param world The world
     * @param box A bounding box of the area
     * @param imageConsumer What to do with the image?
     */
    @Asynchronous
    public void createMap(@NotNull World world, @NotNull BoundingBox box, @NotNull Consumer<BufferedImage> imageConsumer) {
        Location[] bounds = new Location[2];
        bounds[0] = box.getMin().toLocation(world);
        bounds[1] = box.getMax().toLocation(world);
        Map map = new MapAccessible(bounds, imageConsumer);
        new MapProcessor(map);
    }

    @Asynchronous
    public void createMap(@NotNull Location corner1, @NotNull Location corner2, @NotNull Consumer<BufferedImage> imageConsumer) {
        createMap(corner1.getWorld(), BoundingBox.of(corner1, corner2), imageConsumer);
    }

    @Asynchronous
    public void createMap(@NotNull World world, @NotNull BoundingBox box, File file) {
        Location[] bounds = new Location[2];
        bounds[0] = box.getMin().toLocation(world);
        bounds[1] = box.getMax().toLocation(world);
        FileManager.putIfAbsent(file);
        Map map = new MapAccessible(bounds, file);
        new MapProcessor(map);
    }

    @Asynchronous
    public void createFastMap(@NotNull World world, @NotNull BoundingBox box, File file) {
        FileManager.putIfAbsent(file);
        MapDivisible mapDivisible = new MapDivisible(world, box, 24, file);
        new MapProcessor(mapDivisible);
    }

    @Asynchronous
    public void createMap(@NotNull Location corner1, @NotNull Location corner2, File file) {
        createMap(corner1.getWorld(), BoundingBox.of(corner1, corner2), file);
    }

    @Asynchronous
    public void generateMap(@NotNull File folder, Chunk... chunks) {
        if (!folder.exists())
            folder.mkdirs();
        if (!folder.isDirectory()) throw new IllegalArgumentException();
        for (Chunk chunk : chunks) {
            String name = (chunk.getX() + "_" + chunk.getZ()).trim() + ".png";
            File file = new File(folder, name);
            FileManager.putIfAbsent(file);
            new MapProcessor(new MapChunk(chunk, file));
        }
    }

    @Asynchronous
    public void generateMap(@NotNull File folder, @NotNull World world) {
        if (!folder.exists())
            folder.mkdirs();
        if (!folder.isDirectory()) throw new IllegalArgumentException();
        WorldChunkIterator iterator = new WorldChunkIterator(world, (chunks -> {
            for (Chunk chunk : chunks) {
                String name = (chunk.getX() + "_" + chunk.getZ()).trim() + ".png";
                File file = new File(folder, name);
                FileManager.putIfAbsent(file);
                new MapProcessor(new MapChunk(chunk, file));
            }
        }));
        iterator.start();
    }

    protected class MapProcessor implements Runnable {

        private final @NotNull Map map;

        public MapProcessor(@NotNull Map map) {
            this.map = map;
            threads.execute(this);
        }

        @Override
        public void run() {
            addProgressor();
            map.collect();
            removeProgressor();
        }
    }

}
