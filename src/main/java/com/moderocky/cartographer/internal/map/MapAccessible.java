package com.moderocky.cartographer.internal.map;

import com.moderocky.cartographer.internal.map.overlay.Icon;
import com.moderocky.cartographer.internal.map.template.DecoratedMap;
import com.moderocky.cartographer.internal.map.template.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class MapAccessible implements Runnable, Map, DecoratedMap {

    private final Consumer<BufferedImage> imageConsumer;
    private final Instant startTime = Instant.now();
    private Location[] bounds;
    private BufferedImage image;
    private List<Icon> icons = new ArrayList<>();

    public MapAccessible(Location[] bounds, Consumer<BufferedImage> imageConsumer) {
        this.bounds = bounds;
        this.imageConsumer = imageConsumer;
        this.image = null;
    }

    public MapAccessible(Location[] bounds, @NotNull File file) {
        this.bounds = bounds;
        this.imageConsumer = bufferedImage -> {
            try {
                ImageIO.write(bufferedImage, "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        this.image = null;
    }

    @Override
    public void collect() {
        run();
    }

    @Override
    public void run() {
        Bukkit.broadcastMessage("Map starting!"); //TODO
        int w = bounds[1].getBlockX() - bounds[0].getBlockX() + 1;
        int h = bounds[1].getBlockZ() - bounds[0].getBlockZ() + 1;
        int maxX = bounds[1].getBlockX();
        int maxZ = bounds[1].getBlockZ();
        World world = bounds[0].getWorld();
        image = new BufferedImage(w, h, IndexColorModel.OPAQUE);
        int xCount = 0;
        long looooooooong = System.currentTimeMillis();
        for (int x = bounds[0].getBlockX(); x <= maxX; x++) {
            int zCount = 0;
            for (int z = bounds[0].getBlockZ(); z <= maxZ; z++) {
                int col = getColumnColour(world, x, z);
                image.setRGB(xCount, zCount, col);
                zCount++;
            }
            if (System.currentTimeMillis() - looooooooong > 10000) {
                imageConsumer.accept(image);
                looooooooong = System.currentTimeMillis();
            }
            xCount++;
        }
        Bukkit.broadcastMessage("Map complete: " + Duration.between(startTime, Instant.now()).getSeconds() + "s"); //TODO

//        icons.addAll(collectStructureIcons(world, BoundingBox.of(bounds[0], bounds[1])));
//        icons.addAll(collectSpawnIcon(world, BoundingBox.of(bounds[0], bounds[1])));
//        applyStructureIcons(icons, image);
        imageConsumer.accept(image);
    }

    public BufferedImage getImage() {
        return image;
    }

    @Override
    public World getWorld() {
        return bounds[0].getWorld();
    }

    @Override
    public BoundingBox getArea() {
        return BoundingBox.of(bounds[0], bounds[1]);
    }
}
