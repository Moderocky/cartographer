package com.moderocky.cartographer.internal.map.template;

import com.moderocky.cartographer.internal.map.WorldChunkIterator;
import com.moderocky.cartographer.internal.map.overlay.Icon;
import org.bukkit.Location;
import org.bukkit.StructureType;
import org.bukkit.World;
import org.bukkit.util.BoundingBox;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DecoratedMap extends Map {

    default void applyStructureIcons(List<Icon> icons, BufferedImage image) {
        Graphics2D graphics = image.createGraphics();
        for (Icon icon : icons) {
            graphics.drawImage(icon.getType().getImage(), Math.min(image.getHeight() - 8, Math.max(0, icon.getX() - 4)), Math.min(image.getHeight() - 8, Math.max(0, icon.getZ() - 4)), null);
        }
        graphics.dispose();
    }

    default List<Icon> collectStructureIcons(World world, BoundingBox box) {
        int radius = (int) Math.ceil(Math.max(box.getWidthX(), box.getWidthZ()));
        List<Icon> list = new ArrayList<>();
        Location centre = box.getCenter().toLocation(world);
        List<Location> foundPositions = new ArrayList<>();

        for (StructureType structureType : Arrays.asList(StructureType.OCEAN_MONUMENT, StructureType.WOODLAND_MANSION)) {
            Location test = world.locateNearestStructure(centre, structureType, radius, true);
            if (test == null) continue;
            List<Location> quadrats = WorldChunkIterator.getQuadratPoints(world, box, 128);
            for (Location quadrat : quadrats) {
                Location location = world.locateNearestStructure(centre, structureType, 256, true);
                if (location == null) continue;
                if (box.contains(location.toVector()) && !foundPositions.contains(location)) {
                    foundPositions.add(location);
                    if (structureType == StructureType.OCEAN_MONUMENT)
                        list.add(new Icon(location.getBlockX(), location.getBlockZ(), Icon.IconType.MONUMENT));
                    else if (structureType == StructureType.WOODLAND_MANSION)
                        list.add(new Icon(location.getBlockX(), location.getBlockZ(), Icon.IconType.MANSION));
                }
            }
        }

        return list;
    }

    default List<Icon> collectSpawnIcon(World world, BoundingBox box) {
        List<Icon> list = new ArrayList<>();

        if (box.contains(world.getSpawnLocation().toVector()))
            list.add(new Icon(world.getSpawnLocation().getBlockX(), world.getSpawnLocation().getBlockZ(), Icon.IconType.SPAWN));

        return list;
    }

}
