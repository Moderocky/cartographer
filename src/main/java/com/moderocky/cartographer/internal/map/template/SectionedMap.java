package com.moderocky.cartographer.internal.map.template;

import org.bukkit.World;
import org.bukkit.util.BoundingBox;

import java.util.List;

public interface SectionedMap extends Map {

    World getWorld();

    List<BoundingBox> getSections();

    @Override
    default BoundingBox getArea() {
        BoundingBox box = new BoundingBox();
        getSections().forEach(box::union);
        return box;
    }

}
