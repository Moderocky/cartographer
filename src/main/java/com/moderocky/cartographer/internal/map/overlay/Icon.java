package com.moderocky.cartographer.internal.map.overlay;

import com.moderocky.cartographer.Cartographer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class Icon {

    private final int x;
    private final int z;
    private final IconType type;

    public Icon(int x, int z, IconType type) {
        this.x = x;
        this.z = z;
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public IconType getType() {
        return type;
    }

    public enum IconType {

        MONUMENT("Ocean Monument", "icons/monument.png"),
        MANSION("Mansion", "icons/mansion.png"),
        SPAWN("Spawn", "icons/spawn.png"),
        CROSS("Cross", "icons/cross.png");

        private String name;
        private String path;
        private BufferedImage image;

        IconType(String name, String path) {
            this.name = name;
            this.path = path;
            this.image = null;
        }

        public String getName() {
            return name;
        }

        public String getPath() {
            return path;
        }

        public BufferedImage getImage() {
            if (image != null) return image;
            try {
                InputStream stream = Cartographer.getInstance().getResource("icons/monument.png");
                if (stream != null) {
                    image = ImageIO.read(stream);
                    return image;
                }
            } catch (IOException e) {
                //
            }
            throw new NullPointerException();
        }
    }

}
