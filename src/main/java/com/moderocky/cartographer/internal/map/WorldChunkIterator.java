package com.moderocky.cartographer.internal.map;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WorldChunkIterator extends Thread implements Runnable {

    private final @NotNull World world;
    private final @NotNull List<Chunk> chunks = new ArrayList<>();
    private @Nullable Consumer<Chunk[]> chunkConsumer;

    public WorldChunkIterator(@NotNull World world, @Nullable Consumer<Chunk[]> chunkConsumer) {
        this.world = world;
        this.chunkConsumer = chunkConsumer;
    }

    public static List<Location> getQuadratPoints(World world, BoundingBox box, double size) {
        List<Location> quadrats = new ArrayList<>();
        for (double x = box.getMinX(); x <= box.getMaxX(); x = x + size) {
            for (double z = box.getMinZ(); z <= box.getMaxZ(); z = z + size) {
                quadrats.add(new Location(world, x, 60, z));
            }
        }
        return quadrats;
    }

    public static List<BoundingBox> getQuadrats(World world, BoundingBox box, int total) {
        List<BoundingBox> quadrats = new ArrayList<>();
        double minX = box.getMinX();
        double minY = box.getMinY();
        double minZ = box.getMinZ();
        double maxY = box.getMaxY();
        double maxZ = box.getMaxZ();
        int factor = (int) Math.floor(box.getWidthX() / total);
        double prevPosX = box.getMaxX();
        double posX = box.getMaxX();
        for (int i = 0; i < total; i++) {
            posX = posX - factor;
            quadrats.add(box.clone().resize(posX, minY, minZ, prevPosX, maxY, maxZ));
            prevPosX = posX;
        }
        if (posX > minX)
            quadrats.add(box.clone().resize(minX, minY, minZ, posX, maxY, maxZ));
        return quadrats;
    }

    @Override
    public void run() {
        final Pattern regionPattern = Pattern.compile("r\\.([0-9-]+)\\.([0-9-]+)\\.mca");

        File worldDir = new File(Bukkit.getWorldContainer(), world.getName());
        File regionDir = new File(worldDir, "region");

        File[] regionFiles = regionDir.listFiles((dir, name) -> regionPattern.matcher(name).matches());

        if (regionFiles == null) return;
        for (File f : regionFiles) {
            Matcher matcher = regionPattern.matcher(f.getName());
            if (!matcher.matches()) {
                continue;
            }

            int mcaX = Integer.parseInt(matcher.group(1));
            int mcaZ = Integer.parseInt(matcher.group(2));

            for (int cx = 0; cx < 32; cx++) {
                for (int cz = 0; cz < 32; cz++) {
                    if (world.isChunkGenerated((mcaX << 5) + cx, (mcaZ << 5) + cz))
                        chunks.add(world.getChunkAt((mcaX << 5) + cx, (mcaZ << 5) + cz));
                }
            }
        }
        if (chunkConsumer != null)
            chunkConsumer.accept(chunks.toArray(new Chunk[0]));
    }

}
