package com.moderocky.cartographer.internal.map;

import com.moderocky.cartographer.Cartographer;
import com.moderocky.cartographer.internal.blind.BlindDataInterface;
import com.moderocky.mask.internal.utility.FileManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

public final class Palette {

    private final @NotNull HashMap<String, Integer> colourMap = new HashMap<>();

    public Palette(FileConfiguration configuration) {
        regenerate(configuration);
    }

    public void generate() {
        File file = new File("plugins/Cartographer/", "colours.yml");
        FileManager.putIfAbsent(file);
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        Block block = Bukkit.getWorlds().iterator().next().getBlockAt(0, 1, 0);
        BlockData data = block.getBlockData();
        BlindDataInterface dataInterface = Cartographer.getInstance().getDataInterface();
        for (Material material : Material.values()) {
            if (!material.isBlock()) continue;
            block.setType(material, false);
            int col = dataInterface.getColour(block).asRGB();
            configuration.set("colours." + material.getKey().getKey(), col);
        }
        FileManager.save(configuration, file);
        block.setBlockData(data, false);
        regenerate(configuration);
    }

    public void generateSafely() {
        File file = new File("plugins/Cartographer/", "colours.yml");
        FileManager.putIfAbsent(file);
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        Block block = Bukkit.getWorlds().iterator().next().getBlockAt(0, 1, 0);
        BlockData data = block.getBlockData();
        BlindDataInterface dataInterface = Cartographer.getInstance().getDataInterface();
        for (Material material : Material.values()) {
            if (!material.isBlock()) continue;
            if (configuration.getInt("colours." + material.getKey().getKey(), 0) != 0) continue;
            block.setType(material, false);
            int col = dataInterface.getColour(block).asRGB();
            configuration.set("colours." + material.getKey().getKey(), col);
        }
        FileManager.save(configuration, file);
        block.setBlockData(data, false);
        regenerate(configuration);
    }

    public void regenerate(FileConfiguration configuration) {
        colourMap.clear();
        ConfigurationSection section = configuration.getConfigurationSection("colours");
        if (section == null) {
            generate();
        } else {
            for (String key : section.getKeys(false)) {
                colourMap.putIfAbsent(key, section.getInt(key, 0));
            }
        }
    }

    public void regenerate() {
        File file = new File("plugins/Cartographer/", "colours.yml");
        FileManager.putIfAbsent(file);
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        regenerate(configuration);
    }

    public int getColour(String key) {
        return colourMap.getOrDefault(key, 0);
    }

    public int getColour(Material material) {
        return colourMap.getOrDefault(material.getKey().getKey(), 0);
    }

}
