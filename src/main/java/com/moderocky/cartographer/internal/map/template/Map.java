package com.moderocky.cartographer.internal.map.template;

import com.moderocky.cartographer.Cartographer;
import com.moderocky.cartographer.internal.blind.BlindDataInterface;
import com.moderocky.cartographer.internal.map.Palette;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.util.BoundingBox;

import java.util.Arrays;
import java.util.List;

public interface Map {

    BlindDataInterface dataInterface = Cartographer.getInstance().getDataInterface();
    Palette palette = Cartographer.getInstance().getPalette();
    List<Material> materialList = Arrays.asList(Material.AIR, Material.CAVE_AIR, Material.VOID_AIR, Material.WATER, Material.KELP_PLANT, Material.KELP, Material.SEAGRASS, Material.TALL_SEAGRASS, Material.SEA_PICKLE);

    World getWorld();

    BoundingBox getArea();

    void collect();

    default int getColumnColour(World world, int x, int z) {
        int ay = 255;
        boolean underwater = false;
        Material material;
        int colour;
        do {
            material = dataInterface.getMaterial(world, x, ay, z);
            if (material == Material.WATER) underwater = true;
            if (!underwater && material != Material.AIR && dataInterface.isWaterlogged(world, x, ay, z))
                underwater = true;
            colour = palette.getColour(material);
            ay--;
        } while ((materialList.contains(material) || colour == 0) && ay > 0);
        float shiftFactor = getShiftFactor(new Location(world, x, ay, z));
        int col = shift(colour, shiftFactor);
        if (underwater)
            col = blueShift(col);
        return col;
    }

    default float getShiftFactor(Location location) {
        float shiftFactor = 0.0F;
        if (!dataInterface.getRelative(location, BlockFace.WEST).isSolid() && !dataInterface.getRelative(location, BlockFace.WEST, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + 0.1F;
        }
        if (dataInterface.getRelative(location, BlockFace.WEST, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + -0.1F;
        }
        if (!dataInterface.getRelative(location, BlockFace.NORTH).isSolid() && !dataInterface.getRelative(location, BlockFace.NORTH, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + 0.1F;
        }
        if (dataInterface.getRelative(location, BlockFace.NORTH, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + -0.1F;
        }
        if (!dataInterface.getRelative(location, BlockFace.EAST).isSolid() && !dataInterface.getRelative(location, BlockFace.EAST, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + -0.1F;
        }
        if (dataInterface.getRelative(location, BlockFace.EAST, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + 0.1F;
        }
        if (!dataInterface.getRelative(location, BlockFace.SOUTH).isSolid() && !dataInterface.getRelative(location, BlockFace.SOUTH, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + -0.1F;
        }
        if (dataInterface.getRelative(location, BlockFace.SOUTH, BlockFace.UP).isSolid()) {
            shiftFactor = shiftFactor + 0.1F;
        }
        shiftFactor = shiftFactor * -1;

//        if (dataInterface.getRelative(location, BlockFace.WEST, BlockFace.UP).isSolid()) {
//            shiftFactor = shiftFactor + 0.1F;
//        } else if (!dataInterface.getRelative(location, BlockFace.WEST).isSolid() && !dataInterface.getRelative(location, BlockFace.WEST, BlockFace.DOWN).isSolid()) {
//            shiftFactor = shiftFactor + -0.1F;
//        } else if (dataInterface.getRelative(location, BlockFace.NORTH, BlockFace.UP).isSolid()) {
//            shiftFactor = shiftFactor + 0.1F;
//        } else if (!dataInterface.getRelative(location, BlockFace.NORTH).isSolid() && !dataInterface.getRelative(location, BlockFace.NORTH, BlockFace.DOWN).isSolid()) {
//            shiftFactor = shiftFactor + -0.1F;
//        } else if (dataInterface.getRelative(location, BlockFace.SOUTH, BlockFace.UP).isSolid()) {
//            shiftFactor = shiftFactor + -0.1F;
//        } else if (!dataInterface.getRelative(location, BlockFace.SOUTH).isSolid() && !dataInterface.getRelative(location, BlockFace.SOUTH, BlockFace.DOWN).isSolid()) {
//            shiftFactor = shiftFactor + 0.1F;
//        } else if (dataInterface.getRelative(location, BlockFace.EAST, BlockFace.UP).isSolid()) {
//            shiftFactor = shiftFactor + -0.1F;
//        } else if (!dataInterface.getRelative(location, BlockFace.EAST).isSolid() && !dataInterface.getRelative(location, BlockFace.EAST, BlockFace.DOWN).isSolid()) {
//            shiftFactor = shiftFactor + 0.1F;
//        }
        return shiftFactor;
    }

    default int shift(Color colour, float factor) {
        factor = 1 - factor;
        colour = colour.setRed(Math.min(255, Math.max(0, Math.round(colour.getRed() * factor))));
        colour = colour.setGreen(Math.min(255, Math.max(0, Math.round(colour.getGreen() * factor))));
        colour = colour.setBlue(Math.min(255, Math.max(0, Math.round(colour.getBlue() * factor))));
        return colour.asRGB();
    }

    default int shift(int col, float factor) {
        Color colour = Color.fromRGB(col);
        factor = 1 - factor;
        colour = colour.setRed(Math.min(255, Math.max(0, Math.round(colour.getRed() * factor))));
        colour = colour.setGreen(Math.min(255, Math.max(0, Math.round(colour.getGreen() * factor))));
        colour = colour.setBlue(Math.min(255, Math.max(0, Math.round(colour.getBlue() * factor))));
        return colour.asRGB();
    }

    default int blueShift(int col) {
        Color colour = Color.fromRGB(col);
        float factor = 0.4F;
        colour = colour.setRed(Math.min(255, Math.max(0, Math.round(colour.getRed() * factor))));
        colour = colour.setGreen(Math.min(255, Math.max(0, Math.round(colour.getGreen() * factor))));
        colour = colour.setBlue(Math.min(255, Math.max(0, colour.getBlue() + 70)));
        return colour.asRGB();
    }

}
