package com.moderocky.cartographer.internal.map;

import com.moderocky.cartographer.internal.map.overlay.Icon;
import com.moderocky.cartographer.internal.map.template.DecoratedMap;
import com.moderocky.cartographer.internal.map.template.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class MapDivisible implements Runnable, Map, DecoratedMap {

    private final Consumer<BufferedImage> imageConsumer;
    private final @NotNull BoundingBox box;
    private final @NotNull World world;
    private final Instant startTime = Instant.now();
    private Location[] bounds;
    private BufferedImage image;
    private List<Icon> icons = new ArrayList<>();
    private int channels;
    private int progress = 0;

    public MapDivisible(@NotNull World world, @NotNull BoundingBox box, int channels, Consumer<BufferedImage> imageConsumer) {
        this.bounds = new Location[]{box.getMin().toLocation(world), box.getMax().toLocation(world)};
        this.world = world;
        this.channels = channels;
        this.imageConsumer = imageConsumer;
        this.image = null;
        this.box = box;
    }

    public MapDivisible(@NotNull World world, @NotNull BoundingBox box, int channels, @NotNull File file) {
        this.bounds = new Location[]{box.getMin().toLocation(world), box.getMax().toLocation(world)};
        this.world = world;
        this.box = box;
        this.channels = channels;
        this.imageConsumer = bufferedImage -> {
            try {
                ImageIO.write(bufferedImage, "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        this.image = null;
    }

    @Override
    public void collect() {
        run();
    }

    public void increment() {
        progress++;
        imageConsumer.accept(image);
        if (progress < box.getWidthX()) return;
        imageConsumer.accept(image);

        Bukkit.broadcastMessage("Map complete: " + Duration.between(startTime, Instant.now()).getSeconds() + "s"); //TODO
    }

    @Override
    public void run() {
        Bukkit.broadcastMessage("Split map starting!"); //TODO
        int w = bounds[1].getBlockX() - bounds[0].getBlockX() + 1;
        int h = bounds[1].getBlockZ() - bounds[0].getBlockZ() + 1;
        int minX = bounds[0].getBlockX();
        int minZ = bounds[0].getBlockZ();
        int maxX = bounds[1].getBlockX();
        int maxZ = bounds[1].getBlockZ();
        World world = bounds[0].getWorld();
        image = new BufferedImage(w, h, IndexColorModel.OPAQUE);
        int xCount = 0;
        for (int x = bounds[0].getBlockX(); x <= maxX; x++) {
            new MapLine(new Location(world, x, 255, minZ), xCount).spawn();
            try {
                Thread.sleep(1200);
            } catch (InterruptedException e) {
                //
            }
            xCount++;
        }
    }

    public BufferedImage getImage() {
        return image;
    }

    @NotNull
    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public BoundingBox getArea() {
        return BoundingBox.of(bounds[0], bounds[1]);
    }

    public class MapLine extends Thread implements Runnable {
        private int startX;
        private Location start;

        protected MapLine(Location start, int startX) {
            this.startX = startX;
            this.start = start;
        }

        public void spawn() {
            start();
        }

        @Override
        public void run() {
            int length = image.getHeight() - 1;
            int zCount = 0;
            for (int z = start.getBlockZ(); z <= (start.getBlockZ() + (length)); z++) {
                int col = getColumnColour(world, start.getBlockX(), z);
                image.setRGB(startX, Math.min(length, zCount), col);
                zCount++;
            }
            increment();
        }

    }

    public class MapSubsection extends Thread implements Runnable {
        private int startX;
        private int startZ;
        private BoundingBox box;

        protected MapSubsection(BoundingBox box, int startX, int startZ) {
            this.startX = startX;
            this.startZ = startZ;
            this.box = box;
        }

        public void spawn() {
            start();
        }

        @Override
        public void run() {
            int xCount = startX;
            int maxX = (int) Math.floor(box.getMaxX());
            int maxZ = (int) Math.floor(box.getMaxZ());
            for (int x = (int) Math.floor(box.getMinX()); x <= maxX; x++) {
                int zCount = startZ;
                for (int z = (int) Math.floor(box.getMinZ()); z <= maxZ; z++) {
                    int col = getColumnColour(world, x, z);
                    Bukkit.broadcastMessage(xCount + " x/z " + zCount); //TODO
                    image.setRGB(Math.max(0, Math.min(image.getWidth() - 1, xCount)), Math.max(0, Math.min(image.getHeight() - 1, zCount)), col);
                    zCount++;
                }
                xCount++;
            }
            increment();
        }

    }
}
