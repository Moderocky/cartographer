package com.moderocky.cartographer.internal.map;

import com.moderocky.cartographer.Cartographer;
import com.moderocky.cartographer.internal.blind.BlindDataInterface;
import com.moderocky.cartographer.internal.map.template.Map;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.BoundingBox;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

public class MapChunk implements Runnable, Map {

    private Chunk chunk;
    private BlindDataInterface dataInterface = Cartographer.getInstance().getDataInterface();
    private Consumer<BufferedImage> imageConsumer;

    public MapChunk(Chunk chunk, Consumer<BufferedImage> imageConsumer) {
        this.chunk = chunk;
        this.imageConsumer = imageConsumer;
    }

    public MapChunk(Chunk chunk, File file) {
        this.chunk = chunk;
        this.imageConsumer = bufferedImage -> {
            try {
                ImageIO.write(bufferedImage, "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }

    @Override
    public void collect() {
        run();
    }

    @Override
    public void run() {
        BufferedImage bufferedImage = new BufferedImage(16, 16, IndexColorModel.OPAQUE);
        World world = chunk.getWorld();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                Block block = chunk.getBlock(x, 255, z);
                int col = getColumnColour(world, block.getX(), block.getZ());
                bufferedImage.setRGB(x, z, col);
            }
        }
        imageConsumer.accept(bufferedImage);
    }

    @Override
    public World getWorld() {
        return chunk.getWorld();
    }

    @Override
    public BoundingBox getArea() {
        return BoundingBox.of(chunk.getBlock(0, 0, 0).getLocation(), chunk.getBlock(15, 255, 15).getLocation());
    }

}
