package com.moderocky.cartographer.internal.command;

import com.moderocky.cartographer.Cartographer;
import com.moderocky.mask.template.CompleteCommand;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MapCommand implements CompleteCommand {
    @Override
    public @NotNull String getCommand() {
        return "map";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) {
        if (i == 1)
            return Arrays.asList("chunks", "area", "splitarea", "regeneratepalette", "worldmap");
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;
        if (args.length > 0 && args[0].equalsIgnoreCase("chunks")) {
            int i;
            try {
                if (args.length > 1)
                    i = Math.max(Math.min(Integer.parseInt(args[1]), 100), 1);
                else
                    i = 1;
            } catch (Exception e) {
                i = 1;
            }
            String worldName = player.getWorld().getName().trim();
            Chunk centre = player.getChunk();
            List<Chunk> chunks = new ArrayList<>();
            for (int x = centre.getX() - i; x < centre.getX() + i; x++) {
                for (int z = centre.getZ() - i; z < centre.getZ() + i; z++) {
                    Chunk chunk = centre.getWorld().getChunkAt(x, z);
                    chunks.add(chunk);
                }
            }
            Cartographer.getAPI().generateMap(new File("plugins/Cartographer/map/" + worldName + "/"), chunks.toArray(new Chunk[0]));
        } else if (args.length > 0 && args[0].equalsIgnoreCase("area")) {
            if (args.length < 3) return false;
            int x;
            int z;
            try {
                x = Math.max(Math.min(Integer.parseInt(args[1]), 2048), 16);
                z = Math.max(Math.min(Integer.parseInt(args[2]), 2048), 16);
            } catch (Exception e) {
                x = 128;
                z = 128;
            }
            Location l1 = new Location(player.getWorld(), player.getLocation().getBlockX() - (x / 2.0F), 0, player.getLocation().getBlockZ() - (z / 2.0F));
            Location l2 = new Location(player.getWorld(), player.getLocation().getBlockX() + (x / 2.0F), 0, player.getLocation().getBlockZ() + (z / 2.0F));
            String name = "gen_" + (new Random().nextInt(899999) + 100000) + ".png";
            File file = new File("plugins/Cartographer/map/generated/", name);
            player.sendMessage("Beginning generation: " + name);
            Cartographer.getAPI().createMap(l1, l2, file);
        } else if (args.length > 0 && args[0].equalsIgnoreCase("worldmap")) {
            Cartographer.getAPI().generateMap(new File("plugins/Cartographer/map/" + player.getWorld().getName() + "/"), player.getWorld());
            player.sendMessage("Beginning generation of world map.");
        } else if (args.length > 0 && args[0].equalsIgnoreCase("splitarea")) {
            if (args.length < 3) return false;
            int x;
            int z;
            try {
                x = Math.max(Math.min(Integer.parseInt(args[1]), 2048), 16);
                z = Math.max(Math.min(Integer.parseInt(args[2]), 2048), 16);
            } catch (Exception e) {
                x = 128;
                z = 128;
            }
            Location l1 = new Location(player.getWorld(), player.getLocation().getBlockX() - (x / 2.0F), 0, player.getLocation().getBlockZ() - (z / 2.0F));
            Location l2 = new Location(player.getWorld(), player.getLocation().getBlockX() + (x / 2.0F), 0, player.getLocation().getBlockZ() + (z / 2.0F));
            String name = "gen_" + (new Random().nextInt(899999) + 100000) + ".png";
            File file = new File("plugins/Cartographer/map/generated/", name);
            player.sendMessage("Beginning fast generation: " + name);
            Cartographer.getAPI().createFastMap(player.getWorld(), BoundingBox.of(l1, l2), file);
        } else if (args.length > 0 && args[0].equalsIgnoreCase("regeneratepalette")) {
            player.sendMessage("Regenerating palette.");
            Cartographer.getAPI().regeneratePalette();
        } else {
            return false;
        }
        return true;
    }
}
