package com.moderocky.cartographer.internal.command;

import com.moderocky.mask.template.CompleteCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class CartographerCommand implements CompleteCommand {
    @Override
    public @NotNull String getCommand() {
        return "cartographer";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) {
        if (i == 1)
            return Arrays.asList("help", "update");
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length > 0 && args[0].equalsIgnoreCase("update")) {

        } else {
            sender.sendMessage(new ComponentBuilder("")
                    .append("‹")
                    .color(ChatColor.DARK_GRAY)
                    .append("Cartographer")
                    .color(ChatColor.DARK_PURPLE)
                    .append("›")
                    .color(ChatColor.DARK_GRAY)
                    .append(" ")
                    .reset()
                    .append("Displaying help for the Cartographer API:")
                    .color(ChatColor.WHITE)
                    .append(System.lineSeparator())
                    .append("/map (chunks <rad>/area <dx> <dz>)")
                    .color(ChatColor.GRAY)
                    .create()
            );
        }
        return true;
    }
}
