package com.moderocky.cartographer.internal.web;

import com.moderocky.cartographer.internal.web.listener.PostListener;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import com.moderocky.mask.api.web.event.PostEvent;

import java.net.Socket;

public class MapServer extends WebServer<WebConnection> {

    public MapServer(int port, String webContentRoot) {
        super(port, webContentRoot);
        registerListener(PostEvent.class, new PostListener());
    }

    @Override
    public WebConnection createConnection(Socket socket) {
        return new WebConnection(socket, this) {};
    }
}
