package com.moderocky.cartographer.internal.web.listener;

import com.moderocky.mask.api.web.WebRequestListener;
import com.moderocky.mask.api.web.event.PostEvent;
import com.moderocky.mask.api.web.event.WebActionEvent;
import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PostListener implements WebRequestListener<PostEvent> {

    @Override
    public void onEvent(PostEvent event) {
        if (!event.hasInputStream()) return;
        BufferedReader reader = event.getReader();
        try {
            reader.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 30; i++) {
            try {
                String line;
                if ((line = reader.readLine()) != null) {
                    Bukkit.broadcastMessage(line);
                }
            } catch (Throwable ignore) {}
        }
        return;
//        for (int i = 0; i < 10; i++) {
//            try {
//                if (reader.readLine() == null) break;
//            } catch (IOException ignore) {}
//        }
//        for (int i = 0; i < 10; i++) {
//            try {
//                if (reader.readLine() == null) break;
//            } catch (IOException ignore) {}
//        }
//        StringBuilder payload = new StringBuilder();
//        for (int i = 0; i < 100; i++) {
//            try {
//                if (!reader.ready()) break;
//                payload.append((char) reader.read());
//            } catch (IOException e) {
//                //
//            }
//        }
//        String result = payload.toString().trim();
//        Bukkit.broadcastMessage(result);
    }

}
