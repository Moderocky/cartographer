package com.moderocky.cartographer.internal.config;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.template.Config;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class PluginConfig implements Config {

    @Configurable
    public boolean enableMetrics = true;

    @Configurable
    public boolean mapAllWorlds = false;

    @Configurable
    public boolean enableWebMap = true;

    @Configurable
    @Configurable.Bounded(minValue = 0, maxValue = 70000)
    public int webMapPort = 8500;

    @Configurable
    public List<String> mappedWorlds = Arrays.asList("world", "world_the_end", "your_world_name_here");

    public PluginConfig() {
        load();
    }

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Cartographer/";
    }

    @Override
    public @NotNull String getFileName() {
        return "config.yml";
    }

}
