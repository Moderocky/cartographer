package com.moderocky.cartographer.internal.blind.v1_15_R1;

import com.moderocky.cartographer.internal.blind.BlindDataInterface;
import net.minecraft.server.v1_15_R1.*;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_15_R1.block.data.CraftBlockData;
import org.bukkit.craftbukkit.v1_15_R1.util.CraftMagicNumbers;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.HashMap;

public class DataInterface implements BlindDataInterface {

    private final @NotNull HashMap<Material, Color> colorHashMap = new HashMap<>();

    @Override
    public Color getColour(Material material) {
        if (colorHashMap.containsKey(material)) return colorHashMap.get(material);
        Block block = ((CraftBlockData) material.createBlockData()).getState().getBlock();
        try {
            Field field = block.getClass().getDeclaredField("t");
            if (!field.isAccessible()) field.setAccessible(true);
            MaterialMapColor mapColor = (MaterialMapColor) field.get(block);
            Color color = Color.fromRGB(mapColor.rgb);
            colorHashMap.putIfAbsent(material, color);
            return color;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            return Color.fromRGB(0);
        }
    }

    @Override
    public int getApproximateColour(Material material) {
        return ((CraftBlockData) material.createBlockData()).getState().getMaterial().i().rgb;
    }

    @Override
    public Color getColour(org.bukkit.block.Block block) {
        BlockPosition position = new BlockPosition(block.getX(), block.getY(), block.getZ());
        WorldServer worldServer = ((CraftWorld) block.getWorld()).getHandle();
        IBlockData data = worldServer.getType(position);
        return Color.fromRGB(data.c(worldServer, position).rgb);
    }

    @Override
    public boolean isFluid(org.bukkit.block.Block block) {
        BlockPosition position = new BlockPosition(block.getX(), block.getY(), block.getZ());
        WorldServer worldServer = ((CraftWorld) block.getWorld()).getHandle();
        IBlockData data = worldServer.getType(position);
        return !data.getFluid().isEmpty();
    }

    @Override
    public boolean isWaterlogged(org.bukkit.block.Block block) {
        BlockPosition position = new BlockPosition(block.getX(), block.getY(), block.getZ());
        WorldServer worldServer = ((CraftWorld) block.getWorld()).getHandle();
        IBlockData data = worldServer.getType(position);
        return data.getFluid().getType() instanceof FluidTypeWater;
    }

    @Override
    public void setBlock(org.bukkit.World world, int x, int y, int z, int blockId, byte data, boolean applyPhysics) {

        WorldServer nmsWorld = ((CraftWorld) world).getHandle();
        Chunk nmsChunk = nmsWorld.getChunkAt(x >> 4, z >> 4);
        IBlockData ibd = Block.getByCombinedId(blockId + (data << 12));

        ChunkSection cs = nmsChunk.getSections()[y >> 4];
        if (cs == nmsChunk.a()) {
            cs = new ChunkSection(y >> 4 << 4);
            nmsChunk.getSections()[y >> 4] = cs;
        }

        if (applyPhysics)
            cs.getBlocks().setBlock(x & 15, y & 15, z & 15, ibd);
        else
            cs.getBlocks().b(x & 15, y & 15, z & 15, ibd);
    }

    @Override
    public Material getMaterial(Location location) {
        WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
        BlockPosition position = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        IBlockData data = world.getType(position);
        return CraftMagicNumbers.getMaterial(data.getBlock());
    }

    @Override
    public Material getMaterial(org.bukkit.World world, int x, int y, int z) {
        IBlockData data = getIBlockData(world, x, y, z);
        return getMaterial(data);
    }

    @Override
    public boolean isWaterlogged(org.bukkit.World world, int x, int y, int z) {
        IBlockData data = getIBlockData(world, x, y, z);
        if (data.getFluid() != null && data.getFluid() instanceof FluidTypeWater)
            return true;
        Material material = getMaterial(data);
        return material == Material.TALL_SEAGRASS || material == Material.SEAGRASS || material == Material.KELP_PLANT;
    }

    @Override
    public Material getRelative(Location location, BlockFace... faces) {
        Location locus = location.clone();
        for (BlockFace face : faces) {
            locus = getRelativeLocation(locus, face);
        }
        return getMaterial(locus);
    }

    @Override
    public Location getRelativeLocation(Location location, BlockFace face) {
        return getRelativeLocation(location, face, 1);
    }

    @Override
    public Location getRelativeLocation(Location location, BlockFace face, int distance) {
        location = location.clone();
        location.set(location.getBlockX() + face.getModX() * distance, location.getBlockY() + face.getModY() * distance, location.getBlockZ() + face.getModZ() * distance);
        return location;
    }

    @Override
    public Material getRelative(Location location, BlockFace face) {
        return getRelative(location, face, 1);
    }

    @Override
    public Material getRelative(Location location, BlockFace face, int distance) {
        return getMaterial(getRelativeData(location, face, distance));
    }

    private IBlockData getIBlockData(org.bukkit.World world, int x, int y, int z) {
        WorldServer nmsWorld = ((CraftWorld) world).getHandle();
        return getIBlockData(nmsWorld, x, y, z);
    }

    private IBlockData getIBlockData(WorldServer nmsWorld, int x, int y, int z) {
        Chunk nmsChunk = nmsWorld.getChunkAt(x >> 4, z >> 4);
        return nmsChunk.getType(new BlockPosition(x, y, z));
//        ChunkSection cs = nmsChunk.getSections()[y >> 4];
//        if (cs == nmsChunk.a()) {
//            cs = new ChunkSection(y >> 4 << 4);
//            nmsChunk.getSections()[y >> 4] = cs;
//        }
//        return cs.getType(x & 15, y & 15, z & 15);
    }

    private IBlockData getRelativeData(Location location, final int modX, final int modY, final int modZ) {
        WorldServer world = getWorldServer(location);
        return getIBlockData(world, location.getBlockX() + modX, location.getBlockY() + modY, location.getBlockZ() + modZ);
    }

    private IBlockData getRelativeData(Location location, BlockFace face, int distance) {
        return getRelativeData(location, face.getModX() * distance, face.getModY() * distance, face.getModZ() * distance);
    }

    private BlockPosition getPosition(Location location) {
        return new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    private WorldServer getWorldServer(Location location) {
        return ((CraftWorld) location.getWorld()).getHandle();
    }

    private Material getMaterial(IBlockData data) {
        return CraftMagicNumbers.getMaterial(data.getBlock());
        //return Material.matchMaterial(data.getBlock().g().toString());
    }

}
