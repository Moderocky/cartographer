package com.moderocky.cartographer.internal.blind;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;

public interface BlindDataInterface {
    Color getColour(Material material);

    int getApproximateColour(Material material);

    Color getColour(org.bukkit.block.Block block);

    boolean isFluid(org.bukkit.block.Block block);

    boolean isWaterlogged(org.bukkit.block.Block block);

    void setBlock(org.bukkit.World world, int x, int y, int z, int blockId, byte data, boolean applyPhysics);

    Material getMaterial(Location location);

    Material getMaterial(org.bukkit.World world, int x, int y, int z);

    boolean isWaterlogged(org.bukkit.World world, int x, int y, int z);

    Material getRelative(Location location, BlockFace... faces);

    Location getRelativeLocation(Location location, BlockFace face);

    Location getRelativeLocation(Location location, BlockFace face, int distance);

    Material getRelative(Location location, BlockFace face);

    Material getRelative(Location location, BlockFace face, int distance);
}
